info
=====

pins
----

// ATMEL ATTINY45 / ARDUINO
//
//                                    +-\/-+
//           Ain0       (D  5)  PB5  1|    |8   VCC
//  input -> Ain3       (D  3)  PB3  2|    |7   PB2  (D  2)  INT0  Ain1
// output <- PWM  Ain2  (D  4)  PB4  3|    |6   PB1  (D  1)        PWM (OC0B) -> RX (debug only)
//           OC1B               GND  4|    |5   PB0  (D  0)        PWM (OC0A) -> TX (debug only)
//                                    +----+


build:
====

el attiny por defecto viene a 1mhz, lo cual es 8 veces menos de su capacidad. 
no obstante a 1mhz consume mucho menos, pero sus fuses hace que su reloj interno funcione mal y 
por ejemplo el sleep no funcione bien (un blink de 1 segundo durará 8)

para solucionarlo hay que actualizar los fuses con:

```bash
pio run -t fuses
```

see: 
fuse calculator https://www.engbedded.com/fusecalc/


serial
-------

usamos softserial para depurar, los pines son los por defecto
