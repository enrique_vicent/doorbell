#include <Arduino.h>

#define INTERNAL2V56_NO_CAP (6)


#define THRESHOLD_CentVolts 85 //0.85v 
#define MV_2_READ_CTE 4 //for 2.56 volts reference (2560 mv = 1023 reads => 1 read = 2.5mv)

int analogPin = A3; //A3 ; gpio2 ; pin2
int outputPin = PB4;
int threshold = THRESHOLD_CentVolts * MV_2_READ_CTE; 

void setup() {
  analogReference( INTERNAL2V56_NO_CAP );
  pinMode(outputPin, OUTPUT);
  pinMode(analogPin, INPUT);
  //Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(analogPin);
  //Serial.println(sensorValue);
  bool active = sensorValue > threshold;
  digitalWrite(outputPin, active?HIGH:LOW);
  if(active) delay(20); //Trigger smith
}