#include <Arduino.h>
#include "coreApi.h"
#include "adapters/log.h"
#include "adapters/leds.h"
#include "adapters/hk_espnow.h"
#include "adapters/watchdog.h"
#include "adapters/solenoid.h"
#include "adapters/bell.h"
#include "adapters/battery.h"
#include "adapters/sleep.h"
#include "adapters/ota.h"

//core to be run
#include "core/core.h"

void setup() {
  core();
  xTaskCreate(logActor, "log", 10000, NULL, 0, NULL);
  xTaskCreate(bell_actor, "bell", 10000, NULL, 1, NULL);
  xTaskCreate(battery_actor, "batt", 10000, NULL, 1, NULL);
  xTaskCreate(leds_actor, "leds", 10000, NULL, 1, NULL);
  xTaskCreate(hk_actor, "espnow", 100000, NULL, 1, NULL);
  xTaskCreate(watchdog_actor, "wdog", 1000, NULL, 1, NULL);
  xTaskCreate(solenoid_actor, "rele", 1000, NULL, 1, NULL);
  xTaskCreate(sleep_actor, "sleep", 10000, NULL, 0, NULL);
  xTaskCreate(ota_actor, "ota", 10000, NULL, 2, NULL);
  doLog("run setup complete");
}

void loop() {
  sleep(portMAX_DELAY);
  //you aren't suposed to reach here! 
  Serial.println("error, core ended");
}