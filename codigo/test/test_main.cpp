#include <Arduino.h>
#include <unity.h>
#include "coreApi.h"
#include "tests/test_battery_poll_ok.hpp"
#include "tests/test_active.hpp"
#include "tests/test_ota.hpp"

void test_test(void){
    TEST_ASSERT_MESSAGE(true, "dummy test works");
}

void setup () {
    delay(500);
    //uncomment next line to get traces
    //xTaskCreate(logActor, "log", 10000, NULL, 0, NULL);
    UNITY_BEGIN();
}

void loop () {
    RUN_TEST(test_test);
    delay(50);
    RUN_TEST(test_battery_poll_noWifi); 
    delay(50);
    RUN_TEST(test_battery_poll_ok);
    delay(50);
    RUN_TEST(test_battery_sleep_while_pending);
    delay(50);
    RUN_TEST(test_active);
    delay(50);
    RUN_TEST(test_active_nowifi);
    delay(50);
    RUN_TEST(test_active_no_hk);
    delay(50);
    RUN_TEST(test_coil);
    delay(50);
    RUN_TEST(test_coil_mid_hk_timeout);
    delay(50);
    RUN_TEST(test_coil_mid_timeout);
    delay(50);
    RUN_TEST(test_coil_leds);
    delay(50);
    RUN_TEST(test_OTA);
    delay(50);
    test::clearToQueues();
    UNITY_END();
}