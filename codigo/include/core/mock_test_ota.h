#ifndef _CORE_H_
#define _CORE_H_

#include <Arduino.h>
#include "coreApi.h"
#include "adapters/log.h"


ota_commands_t comm_start = ota_commands_t::ota_begin;
ota_request_t otaRequest = ota_request_t::ota_start;
ota_updates_t notification;

void core_actor(void * params) {
    delay(1500);
    doLog("* starting");
    //uncomment to do manual enabile, if commentend use mqtt commands
    //xQueueSend(queue_to_ota,&comm_start,100);
    for(;;){
        if (pdTRUE == xQueueReceive(queue_from_ota_friend, &otaRequest, 10)){
            if (otaRequest == ota_request_t::ota_start){
                doLog(">> sending: ota on");
                xQueueSend(queue_to_ota,&comm_start,100);
            } else {
                doLog(">> sending: ota off (reset)");
                esp_restart();
            }
        }
        if (pdTRUE == xQueueReceive(queue_from_ota, &notification, 10)){
            if (notification == ota_updates_t::ota_listening_started){
                doLog("** listening");
            } else {
                doLog("** ++ updating");
            }

        }
    } 
}

void core() {
    xTaskCreate(core_actor, "core", 1000, NULL, 1, NULL);
}

#endif