#pragma once

struct places_t {
    short start, connecting, reading, connected, idle, battery, hk_updating, hk_updated, NOTA, OTA, zzz, ticking, expired, active, opening;
} places;

struct status_t {
    int battery;
} status;

struct model_t {
    status_t status;
    places_t places;

    void initStatus() {
        places.start = 1;
        places.NOTA = 1;
        places.connecting = 0;
        places.reading = 0;
        places.connected = 0;
        places.idle = 0;
        places.battery = 0;
        places.hk_updating = 0;
        places.hk_updated = 0;
        places.OTA = 0;
        places.zzz = 0;
        places.ticking = 0;
        places.expired = 0;
        places.active = 0;
        places.opening = 0;
        status.battery = 0;
    }
    model_t (){
        initStatus();
    }
};

model_t model;