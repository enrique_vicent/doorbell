//fake core to test the leds
#ifndef _CORE_H_
#define _CORE_H_

#include <Arduino.h>
#include "coreApi.h"
#include "adapters/log.h"
#include "config.h"

void core_actor(void * params) {
    watchdog_enum msg_start_brust = watchdog_enum::BRUST_TIMEOUT;
    watchdog_enum msg_start_active = watchdog_enum::ACTIVE_TIMEOUT;
    watchdog_enum msg_watch;
    bool test=false;

    doLog("start 2 timers");
    xQueueSend(queue_to_watchdog__reset, &msg_start_active,0);
    xQueueSend(queue_to_watchdog__reset, &msg_start_brust,0);
    
    test = xQueueReceive(queue_from_watchdog__triggered, &msg_watch, (WATCHDOG_BRUST_SECONDS -1) * 1000);
    doLog(test?"01err":"01ok"); //no llega antes de tiempo

    xQueueSend(queue_to_watchdog__reset, &msg_start_brust,0);
    test = xQueueReceive(queue_from_watchdog__triggered, &msg_watch, (WATCHDOG_BRUST_SECONDS -1) * 1000);
    doLog(test?"02err":"02ok"); //no llega si reseteamos

    test = xQueueReceive(queue_from_watchdog__triggered, &msg_watch, (1.1) * 1000);
    doLog(test?"03ok":"03err"); //finalmente llega
    doLog(msg_watch == msg_start_brust ? "03ok_b":"03wrong");

    test = xQueueReceive(queue_from_watchdog__triggered, &msg_watch, (WATCHDOG_ACTIVE_SECONDS - 2*WATCHDOG_BRUST_SECONDS -3 ) * 1000);
    doLog(test?"11err":"11ok"); //el largo no llega antes de tiempo 
    test = xQueueReceive(queue_from_watchdog__triggered, &msg_watch, (5) * 1000);
    doLog(test?"11ok":"11err"); //llega el largo
    doLog(msg_watch == msg_start_active ? "11ok_b":"11wrong");

    doLog("stop");


    
    for(;;){
        delay(1);
    } 
}

void core() {
    xTaskCreate(core_actor, "core", 1000, NULL, 1, NULL);
}
#endif