#ifndef _CORE_H_
#define _CORE_H_

#include <Arduino.h>
#include "coreApi.h"
#include "adapters/log.h"

hk_events_t incoming;

static const char * tname = "main";

void core_actor(void * params) {
  doLog(tname, "hk pseudo core starting");
  int batt = random(100);
  char logBuffer[32];
  sprintf(logBuffer, "battery %D", batt);
  doLog(tname, logBuffer);
  hk_update_t event;
  
  delay(100);
  doLog(tname, "simulate batt read (random)");
  event.battRead = batt;
  event.updateBatt = true;
  xQueueSend(queue_to_homekit, &event , 0);
  auto result = xQueueReceive(queue_from_homekit, &incoming, 100);
  doLog(tname,(result && incoming==hk_events_t::update_ok)?"1 ack ok [X]":"1 no proper ack [V]");

  //test wifi
  bool wifiMsg= false;
  result = pdTRUE == xQueueReceive(queue_from_wifi, &wifiMsg, 5000);
  doLog(tname,(result && wifiMsg)?"wifiok [V]":"err wifi [X]");
  result = xQueueReceive(queue_from_homekit, &incoming, 100);
  doLog(tname,(result && incoming==hk_events_t::update_ok)?"1 ack ok [V]":"1 no proper ack [X]");

  delay(50);
  doLog(tname, "simulate ringing");
  event.updateBatt = false;
  event.ringing = true;
  xQueueSend(queue_to_homekit, &event , 0);
  result = xQueueReceive(queue_from_homekit, &incoming, 100);
  doLog(tname,(result && incoming==hk_events_t::update_ok)?"2 ack ok [V]":"2 no proper ack [X]");


  event.updateSolenoidStatus = true;
  event.ringing = false;
  event.updateBatt = false;
  for(;;){
    result = xQueueReceive(queue_from_homekit, &incoming, 10);
    if(result){
      switch (incoming)
      {
      case hk_events_t::open_door_comand :
        doLog(tname, "requested oppening door");
        delay(500);
        event.doorIsOpen = true;
        xQueueSend(queue_to_homekit, &event , 0);
        delay(5000);
        event.doorIsOpen = false;
        xQueueSend(queue_to_homekit, &event , 0);

        break;
      case hk_events_t::update_ok :
        doLog(tname, "update completed");
        break;
      //TODO:ota nota events
      default:
        break;
      }
    }
  }
}

void core() {
    xTaskCreate(core_actor, "core", 1000, NULL, 1, NULL);

}

#endif