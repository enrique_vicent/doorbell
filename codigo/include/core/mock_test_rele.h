#ifndef _CORE_H_
#define _CORE_H_

#include <Arduino.h>
#include "coreApi.h"
#include "adapters/log.h"



void core_actor(void * params) {
    const int buttonPin = 13;
    pinMode(buttonPin,INPUT_PULLUP);
    solenoid_id_t msg;
    solenoid_operations_t ack;
    msg.forMilis = 4000;
    msg.open = true;
    
    for(;;){
        if(digitalRead(buttonPin) == LOW) {
            doLog("T","click");
            xQueueSend(queue_to_solenoid,&msg,2000);
            delay(100);
        }
        if(xQueueReceive(queue_from_solenoid, &ack, 0)){
            doLog("T",ack == solenoid_operations_t::solenoid_started? "ack 1" : "ack 2");
        }
        delay(10);
    } 
}

void core() {
    xTaskCreate(core_actor, "core", 1000, NULL, 1, NULL);

}

#endif