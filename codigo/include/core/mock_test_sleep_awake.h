#ifndef _CORE_H_
#define _CORE_H_

#include <Arduino.h>
#include "coreApi.h"
#include "adapters/log.h"
#include "core/mock_test_sleep_awake.h"


void core_actor(void * params) {

    delay(5000);
    //goToSleep
    doLog("command to sleep");
    bool toSleep = true;
    xQueueSend(queue_to_sleep, &toSleep, 10);

    for(;;){
        delay(100);
    } 
}

void core() {
    xTaskCreate(core_actor, "core", 1000, NULL, 1, NULL);
}

#endif