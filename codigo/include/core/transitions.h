#pragma once

#include <Arduino.h>
#include "adapters/log.h"
#include "coreApi.h"
#include "core/core_model.h"
#include "config.h"

//void tryAutomaticTrxSetup();
//void TrxConnected();
//void TrxBatt();


bool autoTrxSetup(){
    if (model.places.start){
        doLog(">>setup");
        watchdog_enum watchdog_msg = watchdog_enum::BRUST_TIMEOUT;
        xQueueSend(queue_to_watchdog__reset, &watchdog_msg, 0);
        model.places.start --;
        model.places.connecting ++;
        model.places.reading ++;
        model.places.idle ++;
        return true;
    }
    return false;
}

bool autoTrxUpdate(){
    if(model.places.connected >0 && model.places.battery > 0){
        doLog(">>update");
        model.places.battery -- ;
        model.places.hk_updating ++ ; //este estado evita que se pueda dormir
        return true;
    }
    return false;
}

bool autoTrxSleep(){
    if(model.places.hk_updated >0 && model.places.idle >0 && model.places.NOTA >0 && model.places.hk_updating == 0){
        doLog(">>sleep");
        bool msg = true;
        xQueueSend(queue_to_sleep, &msg, 0);
        model.places.hk_updated -- ;
        model.places.idle --;
        model.places.NOTA --;
        model.places.zzz ++;
        return true;
    }
    return false;
}

bool autoTrxInactive(){
    if(model.places.expired > 0 && model.places.active > 0 ) {
        doLog(">>inactivate");
        watchdog_enum watchdog_msg = watchdog_enum::BRUST_TIMEOUT;
        xQueueSend(queue_to_watchdog__reset, &watchdog_msg, 0);
        led_options_t ledActiveOff = led_options_t::activeOff;
        xQueueSend(queue_to_led, &ledActiveOff, 0);
        model.places.expired -- ;
        model.places.active -- ;
        model.places.idle ++;
        return true;
    }
    return false;
}

bool tryAutoTrxs(){
    bool result = false , loop = false;
    int cap = 10;
    do
    {
        loop = false;
        loop = loop || autoTrxSetup();
        loop = loop || autoTrxUpdate();
        loop = loop || autoTrxSleep();
        loop = loop || autoTrxInactive();
        result = result || loop;
        cap --;
    } while (loop && cap>0 );
    if(cap==0) doLog("GRAVE; bucle infinito");
    return result;
}

void trxConnected() {
    bool msg;
    xQueueReceive (queue_from_wifi, &msg, 0);
    if(model.places.connecting && msg){    
        led_options_t ledNetworkOn = led_options_t::networkOn;
        xQueueSend(queue_to_led, &ledNetworkOn, 10);
        doLog(">connected");
        model.places.connecting -- ;
        model.places.connected ++;
    }
}


void trxBatt(batt_read_t msg){
    if (model.places.reading){
        doLog(">batt");
        model.status.battery = msg.percent;
        hk_update_t hkUpdate;
        hkUpdate.updateBatt = true;
        hkUpdate.battRead = msg.percent;
        xQueueSend(queue_to_homekit, &hkUpdate, 100);
        model.places.reading --;
        model.places.battery ++;
    }
}

void trxHkDone(){
    if (model.places.hk_updating >0){
        doLog(">hk_done");
        model.places.hk_updating --;
        model.places.hk_updated ++;
    }
}

void trxBrustTimeout(){
    if(model.places.idle > 0 && !model.places.OTA){
        doLog(">brustTimeOut");
        bool msg = true;
        xQueueSend(queue_to_sleep, &msg, 0);
        model.places.NOTA -- ;
        model.places.idle -- ;
        model.places.zzz ++ ;
    }
}

void _trxCall(){
     hk_update_t hkUpdate;
        hkUpdate.ringing = true;
        hkUpdate.updateSolenoidStatus = false;
        hkUpdate.updateBatt = false;
        xQueueSend(queue_to_homekit, &hkUpdate, 100);

        watchdog_enum activeWd = watchdog_enum::ACTIVE_TIMEOUT;
        xQueueSend(queue_to_watchdog__reset, &activeWd, 10);

        led_options_t ledActive = led_options_t::activeOn;
        xQueueSend(queue_to_led, &ledActive, 10);

        setCpuFrequencyMhz(80); //minumun usable freq
}

void trxCall(){
    if(model.places.idle >0 ){
        doLog(">call");
        _trxCall();
        model.places.idle -- ;
        model.places.ticking ++ ;
        model.places.active ++ ;
        model.places.battery ++ ;
    } else if( model.places.ticking > 0){
        doLog(">call_2");
        _trxCall();
        model.places.active -- ;
        model.places.battery ++; 

    }
}

void trxActTimeout(){
    if(model.places.ticking){
        doLog(">actTimeout");

        model.places.ticking -- ;
        model.places.expired ++ ;
    }
}

void trxOpen(){
    if(model.places.active > 0){
        doLog(">opening");
        //open solenoid
        solenoid_id_t solenoidCmd;
        solenoidCmd.open = true;
        solenoidCmd.forMilis = SOLENOID_OPEN_FOR_MILIS;
        xQueueSend(queue_to_solenoid, &solenoidCmd, SOLENOID_OPEN_FOR_MILIS);
        //light solenoid led
        led_options_t coilLedOn = led_options_t::solenoidOn;
        xQueueSend(queue_to_led, &coilLedOn, 100);
        //signal opening door
        hk_update_t hkUpdate;
        hkUpdate.doorIsOpen = true;
        hkUpdate.ringing = false;
        hkUpdate.updateBatt = false;
        hkUpdate.updateSolenoidStatus =true;
        xQueueSend(queue_to_homekit, &hkUpdate, 100);
        //transition
        model.places.active --;
        model.places.opening ++;
        model.places.battery ++;
    }
}

void trxClose(){
    if(model.places.opening > 0){
        doLog(">closing");
        //hkupdate "closed"
        hk_update_t hkUpdate;
        hkUpdate.doorIsOpen = false;
        hkUpdate.ringing = false;
        hkUpdate.updateBatt = false;
        hkUpdate.updateSolenoidStatus =true;
        xQueueSend(queue_to_homekit, &hkUpdate, 100);
        //kill solenoid led
        led_options_t coilLedOn = led_options_t::solenoidOff;
        xQueueSend(queue_to_led, &coilLedOn, 100);
        model.places.opening --;
        model.places.battery ++;
        model.places.active ++;
    }
}

void trxOta(){
    if(model.places.NOTA)
    doLog(">ota");
    ota_commands_t otaOn = ota_commands_t::ota_begin;
    xQueueSend(queue_to_ota,&otaOn,100);
    led_options_t otaLedOn = led_options_t::OTAListening;
    xQueueSend(queue_to_led,&otaLedOn,100);
    model.places.OTA ++; //avoids hibernation
    model.places.NOTA --;
}

void trxOtaUpdating(){
    if(model.places.OTA) {
        doLog(">optaUpdating");
        led_options_t otaLedOn = led_options_t::OTAUpdating;
        xQueueSend(queue_to_led,&otaLedOn,100);
    }
    //no place changes

}

void trxNota(){
    //note, this state ignores current state
    doLog(">reset");
    esp_restart();
}

