//fake core to test the leds
#ifndef _CORE_H_
#define _CORE_H_

#include <Arduino.h>
#include "coreApi.h"
#include "adapters/log.h"
#include "adapters/battery.h"



void core_actor(void * params) {
    delay(2000);
    for(;;){
        xTaskCreate(battery_actor, "batt", 10000, NULL, 1, NULL);
        delay(2000);
    } 
}

void core() {
    xTaskCreate(core_actor, "core", 1000, NULL, 1, NULL);

}

#endif