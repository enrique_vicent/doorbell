//fake core to test the leds
#ifndef _CORE_H_
#define _CORE_H_

#include <Arduino.h>
#include "coreApi.h"

void core_actor(void * params) {
    led_options_t msgs [] = {
        led_options_t::allOn, 
        led_options_t::allOff,
        led_options_t::networkOn,
        led_options_t::activeOn, 
        led_options_t::solenoidOn, 
        led_options_t::solenoidOff, 
        led_options_t::activeOff, 
        led_options_t::networkOff,
        led_options_t::OTAListening,
        led_options_t::OTAUpdating,
        led_options_t::allOff,
    };
    int msgIndex = 0;
    for(;;){
        xQueueSend (queue_to_led, &(msgs[msgIndex]), 0);
        delay(3000);
        msgIndex = (msgIndex +1 ) % (sizeof(msgs) / sizeof(led_options_t));
    } 
}

void core() {
    xTaskCreate(core_actor, "core", 1000, NULL, 1, NULL);
}

#endif