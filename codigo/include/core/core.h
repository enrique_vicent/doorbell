//fake core to test the leds
#ifndef _CORE_H_
#define _CORE_H_

#include <Arduino.h>
#include "coreApi.h"
#include "core/transitions.h"
#include "core/core_model.h"

QueueSetHandle_t core_queueset_incoming = NULL;

template <class T>
T readFromQueue (QueueHandle_t q){
    T result;
    xQueueReceive(q, &result, 0);
    return result;
}

void core_actor(void * params){
    for(;;){
        tryAutoTrxs();
        QueueHandle_t activeQueue;
        activeQueue = (QueueHandle_t) xQueueSelectFromSet(core_queueset_incoming, portMAX_DELAY);
        if (queue_from_wifi == activeQueue){
            trxConnected();
        } else if (queue_from_battery == activeQueue){
            batt_read_t battRead = readFromQueue<batt_read_t>(activeQueue);
            trxBatt(battRead);
        } else if (queue_from_homekit == activeQueue){
            hk_events_t read = readFromQueue<hk_events_t>(activeQueue);
            if(read == hk_events_t::update_ok){
                trxHkDone();
            } else if (read == hk_events_t::open_door_comand){
                trxOpen();
            }
        } else if (queue_from_watchdog__triggered == activeQueue) {
            watchdog_enum dogName = readFromQueue<watchdog_enum>(activeQueue);
            if( dogName == watchdog_enum::BRUST_TIMEOUT){
                trxBrustTimeout();
            } else {
                trxActTimeout();
            }
        } else if (queue_from_bell == activeQueue) {
            readFromQueue<bool>(activeQueue);
            trxCall();
        } else if (queue_from_solenoid == activeQueue){
            solenoid_operations_t solenoidOperation = readFromQueue<solenoid_operations_t>(activeQueue);
            if(solenoidOperation == solenoid_operations_t::solenoid_ended){
                trxClose();
            }
        } else if (queue_from_ota_friend == activeQueue){
            ota_request_t otaCmd = readFromQueue<ota_request_t>(activeQueue); 
            if(otaCmd == ota_request_t::ota_start){
                trxOta();
            }else{
                trxNota();
            }
        } else if (queue_from_ota == activeQueue){
            ota_updates_t otaUpdate = readFromQueue<ota_updates_t>(activeQueue);
            if(otaUpdate == ota_updates_t::ota_updating_firmware){
                trxOtaUpdating();
            }
        } else {
            doLog(">pending! err");
        }
    } 
}

TaskHandle_t coreTaskHandle = NULL;

void core() {
    if (core_queueset_incoming == NULL){
        core_queueset_incoming = _from_queue_set();
    }
    model.initStatus();
    xTaskCreate(core_actor, "core", 1000, NULL, 1, &coreTaskHandle);
}

void destroyCore(){ //put this on a class
    if(coreTaskHandle) vTaskDelete(coreTaskHandle);
}


#endif