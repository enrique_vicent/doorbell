#ifndef CORE_API_H
#define CORE_API_H
#include <Arduino.h>

// leds
enum led_options_t {allOff, allOn , activeOn, activeOff, solenoidOn, solenoidOff, networkOn, networkOff, OTAListening, OTAUpdating};
QueueHandle_t queue_to_led = xQueueCreate( 4, sizeof(led_options_t));

// wifi
QueueHandle_t queue_from_wifi = xQueueCreate( 1, sizeof(bool));  // true = connected, false = unconected


// button
enum button_id_t {btn_call, btn_solenoid};
QueueHandle_t queue_from_button = xQueueCreate( 4, sizeof(button_id_t));

// solenoid
struct solenoid_id_t {
    bool open;
    int forMilis;
};
QueueHandle_t queue_to_solenoid = xQueueCreate( 1, sizeof(solenoid_id_t));

enum solenoid_operations_t {solenoid_started, solenoid_ended};
QueueHandle_t queue_from_solenoid = xQueueCreate( 2, sizeof(solenoid_operations_t));

// ota
enum ota_request_t {ota_start, ota_halt};
QueueHandle_t queue_from_ota_friend = xQueueCreate( 2, sizeof(ota_request_t)); //any adapter enabled to request ota
enum ota_updates_t {ota_listening_started, ota_updating_firmware};
QueueHandle_t queue_from_ota = xQueueCreate( 2, sizeof(ota_updates_t)); //ota feedback
enum ota_commands_t {ota_begin, ota_reset};
QueueHandle_t queue_to_ota = xQueueCreate( 2, sizeof(ota_commands_t));

// battery
struct batt_read_t {
    double volts;
    int percent;
};
QueueHandle_t queue_from_battery = xQueueCreate( 1, sizeof(batt_read_t));

// homekit
struct hk_update_t {
    bool updateBatt = false;
    bool updateSolenoidStatus = false;
    bool ringing = false;
    float battRead = 0.0;
    float doorIsOpen = false;
};
QueueHandle_t queue_to_homekit = xQueueCreate( 10, sizeof(hk_update_t));

enum hk_events_t {update_ok, open_door_comand};
QueueHandle_t queue_from_homekit = xQueueCreate( 10, sizeof(hk_events_t));

// sleep
QueueHandle_t queue_to_sleep = xQueueCreate( 1, sizeof(bool));

// timer
enum watchdog_enum { BRUST_TIMEOUT, ACTIVE_TIMEOUT};
QueueHandle_t queue_to_watchdog__reset =xQueueCreate( 3, sizeof(watchdog_enum));
QueueHandle_t queue_from_watchdog__triggered =xQueueCreate( 3, sizeof(watchdog_enum));

// bell
QueueHandle_t queue_from_bell = xQueueCreate(1, sizeof(bool));

// queueset
QueueSetHandle_t _from_queue_set(){
    QueueSetHandle_t result = xQueueCreateSet(1+4+2+2+2+1+10+3+1);
    xQueueAddToSet(queue_from_wifi, result); //1
    xQueueAddToSet(queue_from_button, result); //4
    xQueueAddToSet(queue_from_solenoid, result); //2
    xQueueAddToSet(queue_from_ota_friend, result); // 2
    xQueueAddToSet(queue_from_ota, result); // 2
    xQueueAddToSet(queue_from_battery, result); //1
    xQueueAddToSet(queue_from_homekit, result); //10
    xQueueAddToSet(queue_from_watchdog__triggered, result); //3
    xQueueAddToSet(queue_from_bell, result); //1
    return result;
};

#endif