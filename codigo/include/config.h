#ifndef _CONFIG_H_
#define _CONFIG_H_

#if __has_include("secrets.h")
#include "secrets.h"
#else
//you should create a secrets.h file with the following declarations
//and include secrets.h in git-secret (or similar)
//WIFI secrets, used to share the channel with your wifi because that is the port used by mqtt
const char* ssid = "wifi_ssid";
const char* password = "your_password";
const char* otapassword = "your_password";
const char* otaHostname = "dorbell";
//espnow secrets
unsigned char gatewayMac[6] = {0x11, 0x22, 0xAA, 0xFF, 0x6FF, 0xFF}; 
unsigned char sharedKey[16] = {11,22,33,44,55,66,77,88,99,100,111,122,133,144,155,166};
unsigned char sharedChannel = 2 ;
#endif

//topics
char * TOPIC_CMD = "cmd";
char * TOPIC_DOOR = "door";
char * TOPIC_BELL = "bell";
char * TOPIC_BATTERY = "battery";

//GPIO pins
const int GPIO_LED_ACTIVE = 18;
const int GPIO_LED_ON =  19;
const int GPIO_LED_OPENING = 5;
const int GPIO_BUTTON = 34;
const int GPIO_ANALOG_BATTERY_METER = 33;
const int GPIO_SOLENOID = 4;
const int GPIO_BELL = 35;

//timeouts
const unsigned long WIFI_CONNECTING_TIMEOUT_MILLIS = 5000;
const unsigned long WATCHDOG_BRUST_SECONDS = 10;
const unsigned long WATCHDOG_ACTIVE_SECONDS = 120;
const unsigned long SOLENOID_OPEN_FOR_MILIS = 3500;
const unsigned long SLEEP_FOR_SECONDS = 12 * 60 * 60;

#endif