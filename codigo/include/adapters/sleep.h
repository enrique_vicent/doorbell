#pragma once

#include <Arduino.h>
#include "adapters/log.h"
#include "coreApi.h"
#include "config.h"

void sleep_actor (void * params){
    bool msg;

    const int ext_wakeup_pin_1 = GPIO_BELL;
    const uint64_t ext_wakeup_pin_1_mask = 1ULL << ext_wakeup_pin_1;
    //const int ext_wakeup_pin_2 = 4;
    //const uint64_t ext_wakeup_pin_2_mask = 1ULL << ext_wakeup_pin_2;

    
    esp_sleep_enable_timer_wakeup(1000L * 1000L * SLEEP_FOR_SECONDS);
    esp_sleep_enable_ext1_wakeup(ext_wakeup_pin_1_mask, ESP_EXT1_WAKEUP_ANY_HIGH); //Note: with this wake up source, you can only use pins that are RTC GPIOs

    for(;;){
        xQueueReceive(queue_to_sleep, &msg, portMAX_DELAY);
        if (msg){
            doLog("going to bed");
            delay(20); //give some tiem to flush serial writes
            esp_deep_sleep_start();
        }
    }
}