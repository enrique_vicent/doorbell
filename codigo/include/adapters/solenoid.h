#pragma once

#include <Arduino.h>
#include "config.h"
#include "coreApi.h"
#include "adapters/log.h"

 
void solenoid_actor (void * parameters){
    solenoid_operations_t ack_on = solenoid_operations_t::solenoid_started;
    solenoid_operations_t ack_off = solenoid_operations_t::solenoid_ended;
    solenoid_id_t command ;

    //enable relay Pin
    pinMode(GPIO_SOLENOID, OUTPUT);
    doLog("waiting");
    for(;;){
        //wait for message
        xQueueReceive(queue_to_solenoid, &command, portMAX_DELAY);
        doLog("command received");
        if (command.open){
            //ack message
            xQueueSend(queue_from_solenoid, &ack_on, 0);
            //activate relay
            digitalWrite(GPIO_SOLENOID, HIGH);
            doLog("open");
            //wait specified time
            delay(command.forMilis);
            //close relay
            digitalWrite(GPIO_SOLENOID, LOW);
            doLog("close");
            //notify message
            xQueueSend(queue_from_solenoid, &ack_off, 100);
        }
    }
}
