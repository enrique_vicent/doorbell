#pragma once

#include <Arduino.h>
#include "config.h"
#include "coreApi.h"
#include "adapters/log.h"

TimerHandle_t thBrust, thActive;
const char * thBrustName = "brust";
const char * thActiveName = "active";
unsigned long thBrustTicks = pdMS_TO_TICKS(WATCHDOG_BRUST_SECONDS * 1000);
unsigned long thActiveTicks = pdMS_TO_TICKS(WATCHDOG_ACTIVE_SECONDS * 1000);


void watchdog_trigger(TimerHandle_t xTimer){
    void * timer = pvTimerGetTimerID( xTimer );
    int utimer = (int) timer;
    watchdog_enum bite = (watchdog_enum) utimer;
    xQueueSend(queue_from_watchdog__triggered, &bite, 0);
    doLog(bite==watchdog_enum::BRUST_TIMEOUT ?"brust!" :"active!");

}

void watchdog_actor (void * parameters){
    thBrust = xTimerCreate( thBrustName, thBrustTicks, pdFALSE, (void *) (watchdog_enum::BRUST_TIMEOUT), watchdog_trigger);
    thActive = xTimerCreate( thActiveName, thActiveTicks, pdFALSE, (void *) (watchdog_enum::ACTIVE_TIMEOUT), watchdog_trigger);
    watchdog_enum command;
    for(;;){
        xQueueReceive(queue_to_watchdog__reset, &command, portMAX_DELAY);
        TimerHandle_t handler = (command == watchdog_enum::BRUST_TIMEOUT) ? thBrust : thActive;
        xTimerStart (handler, 0);
        doLog(command==watchdog_enum::BRUST_TIMEOUT ?"restart brust" :"restart active");
    }
}
