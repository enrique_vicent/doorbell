#pragma once

#include <Arduino.h>
#include "config.h"
#include "coreApi.h"
#include "adapters/log.h"
#include <ArduinoOTA.h>

void InitOTA(const char * hostname);
void setupWiFi(const char* ssid, const char* password);
void endWifi();
void updateStatus_listening();
void updateStatus_connected();
bool connected = false;

void ota_actor (void * parameters){
    ota_commands_t command = ota_commands_t::ota_reset;
    doLog("ota actor sleeping");
    {
        xQueueReceive(queue_to_ota, &command, portMAX_DELAY);
    } while (command != ota_commands_t::ota_begin);
    doLog("ota actor waking up");
    setupWiFi(ssid, password);
    InitOTA(otaHostname);
    doLog("ota actor wake up");
	updateStatus_listening();
    for(;;){
        ArduinoOTA.handle();
        delay(100);
    }
}

void InitOTA(const char * hostname)
{
	// Port defaults to 8266
	// ArduinoOTA.setPort(8266);

	// Hostname defaults to esp8266-[ChipID]
	if(hostname){
		ArduinoOTA.setHostname(hostname);
	}

	// No authentication by default
	ArduinoOTA.setPassword(otapassword);

	// Password can be set with it's md5 value as well
	// MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
	// ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

	ArduinoOTA.onStart([]() {
		String type;
		if (ArduinoOTA.getCommand() == U_FLASH) {
			type = "sketch";
		} else { // U_SPIFFS
			type = "filesystem";
		}

		// NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
		Serial.println("Start updating " + type);
	});

	ArduinoOTA.onEnd([]() {
		Serial.println("\nEnd");
	});

	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
		if(!connected) {
			updateStatus_connected();
			connected = true;
		} 
		//Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
	});

	ArduinoOTA.onError([](ota_error_t error) {
		Serial.printf("Error[%u]: ", error);
		if (error == OTA_AUTH_ERROR) {
			Serial.println("Auth Failed");
		} else if (error == OTA_BEGIN_ERROR) {
			Serial.println("Begin Failed");
		} else if (error == OTA_CONNECT_ERROR) {
			Serial.println("Connect Failed");
		} else if (error == OTA_RECEIVE_ERROR) {
			Serial.println("Receive Failed");
		} else if (error == OTA_END_ERROR) {
			Serial.println("End Failed");
		}
	});

	ArduinoOTA.begin();
	Serial.println("");
	Serial.print("OTA iniciado: ");
    Serial.println(ArduinoOTA.getHostname());
}

void setupWiFi(const char* ssid, const char* password){
    Serial.print("setup wifi .. ");
	WiFi.persistent( false );
    WiFi.begin(ssid, password); 
    Serial.print("wifiConnect ");
    while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
        delay(100);
        Serial.print('.');
    }
    Serial.println("Connection established!");
    Serial.print("IP address:\t");
    Serial.println(WiFi.localIP());  
}

void endWifi(){
    WiFi.disconnect();
    WiFi.mode(WIFI_OFF);
}

void updateStatus_listening(){
	ota_updates_t updating = ota_updates_t::ota_listening_started;
	xQueueSend(queue_from_ota, &updating, 100);
}

void updateStatus_connected(){
	ota_updates_t updating = ota_updates_t::ota_updating_firmware;
	xQueueSend(queue_from_ota, &updating, 100);
}