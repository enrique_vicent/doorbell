#pragma once
#include <Arduino.h>
#include "adapters/log.h"
#include "config.h"
#include "coreApi.h"

#ifdef ESP32
  #include <WiFi.h>
#else
  #include <ESP8266WiFi.h>
#endif

#include "EspNow2MqttClient.hpp"


namespace espnow{
    void getChannel();
    RTC_DATA_ATTR int channel = sharedChannel;
    void onIncomingMsgCallback(response & rsp);
    void addSendBatt(EspNow2MqttClient *client, float read);
    void addSendRing(EspNow2MqttClient *client);
    void addSendDoorStatus(EspNow2MqttClient *client, bool open);
    request requests;
    void createBaseRequest(EspNow2MqttClient *client);
}
namespace hkMessages{
    void sendWifi(bool status);
    void sendAck();
    void sendOpenDoorCommand();
    void sendOta(bool start);
}
void hk_actor(void * params){ 
    espnow::getChannel();

    EspNow2MqttClient client = EspNow2MqttClient("doorbell", sharedKey, gatewayMac, espnow::channel);
    client.onReceiveSomething = espnow::onIncomingMsgCallback;
    espnow::createBaseRequest(&client);
    for(;client.init()!=0;delay(10));
    hkMessages::sendWifi(true);
    
    for(;;){
        espnow::requests.operations_count = 1; //always leave the subscribe operation
        hk_update_t coreRequest;
        auto isNewCoreRequest = xQueueReceive(queue_to_homekit, &coreRequest, 100);
        if (pdTRUE == isNewCoreRequest){
            if (coreRequest.updateBatt){
                espnow::addSendBatt(&client, coreRequest.battRead);
            }
            if (coreRequest.ringing){
                espnow::addSendRing(&client); 
            }
            if (coreRequest.updateSolenoidStatus){
                espnow::addSendDoorStatus(&client, coreRequest.doorIsOpen);
            }
        }
        client.doRequests(espnow::requests);
    }
}
void espnow::getChannel(){
    switch(esp_sleep_get_wakeup_cause()){
        case ESP_SLEEP_WAKEUP_EXT0 : break;  
        case ESP_SLEEP_WAKEUP_EXT1 : break; 
        case ESP_SLEEP_WAKEUP_TIMER : break; 
        case ESP_SLEEP_WAKEUP_TOUCHPAD : break;
        case ESP_SLEEP_WAKEUP_ULP : break;
        default: //update channel only on cold boots
            doLog("cooldboot querying Wi-fi channel");
            if (int32_t n = WiFi.scanNetworks()) {
                for (uint8_t i=0; i<n; i++) {
                    if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
                        espnow::channel = WiFi.channel(i);
                    }
                }
            } 
        break;
    }
}

void espnow::addSendBatt(EspNow2MqttClient *client, float read){
    char buffer[12];
    snprintf(buffer, sizeof buffer, "%4.2f", read);
    requests.operations[requests.operations_count] = client->createRequestOperationSend(buffer, TOPIC_BATTERY);
    requests.operations_count ++;
}

void espnow::addSendRing(EspNow2MqttClient *client){
    char * buffer = "RING";
    requests.operations[requests.operations_count] = client->createRequestOperationSend(buffer, TOPIC_BELL);
    requests.operations_count ++;
}

void espnow::addSendDoorStatus(EspNow2MqttClient *client, bool open){
    char * buffer;
    if (open){
        buffer = (char*)"open";
    } else {
        buffer = (char*)"closed";
    }
    requests.operations[requests.operations_count] = client->createRequestOperationSend(buffer, TOPIC_DOOR);
    requests.operations_count ++;
}

void espnow::onIncomingMsgCallback(response & rsp) {
    //send ack
    if(rsp.opResponses_count > 1){
        hkMessages::sendAck();
    }
    //TODO: process commands
    if(rsp.opResponses[0].result_code == response_Result_OK){ //door command
        char buff[150];
        char * command = rsp.opResponses[0].payload;
        const char open[] = "OPEN";
        auto isOpenCommand = 0 == strncasecmp(command, open, sizeof open );
        if (isOpenCommand) {
            hkMessages::sendOpenDoorCommand();
        } else {
            const char ota[] = "OTA";
            if(0 == strncasecmp(command, ota, sizeof ota )){
                hkMessages::sendOta(true);
            } else {
                const char nota[] = "NOTA";
                if(0 == strncasecmp(command, nota, sizeof nota )){
                    hkMessages::sendOta(false);
                }

            }

        }
        sniprintf(buff, sizeof buff, "incoming command %s", command);
        doLog(buff);
    }
}

void espnow::createBaseRequest(EspNow2MqttClient *client){
    requests = client->createRequest(); 
    requests.operations[0] = client->createRequestOperationSubscribeQueue(TOPIC_CMD);
    requests.operations_count = 1;
}

void hkMessages::sendWifi(bool status){
    xQueueSend(queue_from_wifi, &status, portMAX_DELAY);
    doLog("wifi sent to core");
}

void hkMessages::sendAck(){
    hk_events_t ack = hk_events_t::update_ok;
    xQueueSend(queue_from_homekit, &ack, portMAX_DELAY);
    doLog("Ack sent to core");
}

void hkMessages::sendOpenDoorCommand(){
    hk_events_t cmd = hk_events_t::open_door_comand;
    xQueueSend(queue_from_homekit, &cmd, portMAX_DELAY);
    doLog("doorCommand sent to core");
}

void hkMessages::sendOta(bool start){
    ota_request_t msg = start ? ota_request_t::ota_start : ota_request_t::ota_halt;
    xQueueSend(queue_from_ota_friend, &msg, 100);
    doLog("ota request sent to core");
}



