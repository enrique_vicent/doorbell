#pragma once

#include <Arduino.h>
#include "adapters/log.h"
#include "config.h"

unsigned long lastISRCall = 0;
const unsigned long debounceTime = 2000;
void bellISR();
void registerBellISR();
void CheckWakeupCause();
void bell_actor(void * );
void _nofify_bell();
inline void _nofify_bell_from_ISR();

//imp
void bellISR(){
    unsigned long now = millis();
    if (lastISRCall + debounceTime < now){
        _nofify_bell_from_ISR();
        lastISRCall = now;
        doLog("bell ISR !");
    }
}

void registerBellISR(){
    auto interruptNumber = digitalPinToInterrupt(GPIO_BELL);
    attachInterrupt(interruptNumber, bellISR, RISING);
}

void bell_actor(void * params){
    pinMode(GPIO_BELL, INPUT_PULLDOWN);
    registerBellISR();
    CheckWakeupCause();
    vTaskDelete(NULL); //ending actor
}

void _notify_bell(){
    bool msg = true;
    xQueueSend(queue_from_bell, &msg, 100);
}

inline void _nofify_bell_from_ISR(){
    bool msg = true;
    xQueueSendFromISR(queue_from_bell, &msg, false);
}

void CheckWakeupCause(){
    esp_sleep_wakeup_cause_t wakeup_reason;
    wakeup_reason = esp_sleep_get_wakeup_cause();
    if (wakeup_reason == esp_sleep_wakeup_cause_t::ESP_SLEEP_WAKEUP_EXT1 ){
        _notify_bell();
        doLog("Wakeup by RTC_CNTL"); 
    }
}
