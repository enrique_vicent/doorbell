#ifndef ADAPTER_LOG_H
#define ADAPTER_LOG_H

#include <Arduino.h>

#define ADAPTER_LOG_SOURCE_LENGTH 10
#define ADAPTER_LOG_MESSAGE_LENGTH 32
#define ADAPTER_LOG_QUEUE_LENGTH 16

struct logEvent
{
    char source [ADAPTER_LOG_SOURCE_LENGTH];
    char message [ADAPTER_LOG_MESSAGE_LENGTH];
};

inline logEvent buildLogEvent (const char * source  , const char * message){
    logEvent response;
    strncpy(response.message, message, ADAPTER_LOG_MESSAGE_LENGTH );
    strncpy(response.source, source, ADAPTER_LOG_SOURCE_LENGTH );
    return response;
};

QueueHandle_t logQueue = xQueueCreate( ADAPTER_LOG_QUEUE_LENGTH, sizeof(logEvent));

void doLog (const char * source, const char * message){
    logEvent line = buildLogEvent (source, message);
    xQueueSend (logQueue, &line, 0);
};

void doLog (const char * message){
    TaskHandle_t thisTask = xTaskGetCurrentTaskHandle();
    char * taskName = pcTaskGetTaskName(thisTask);
    doLog(taskName, message);
};


void logActor (void * pvParameters) {
    Serial.begin(115200);
    delay(10);
    Serial.println("log initiated");
    
    for(;;){
        logEvent line;
        xQueueReceive(logQueue, &line, portMAX_DELAY); //wait forever
        Serial.print("[");
        Serial.print(line.source);
        Serial.print("] ");
        Serial.print("(");
        Serial.print(millis());
        Serial.print(") ");
        Serial.print(line.message);
        Serial.println();
    }
}


#endif