#pragma once

#include <Arduino.h>
#include "adapters/log.h"
#include "config.h"

#include <Battery18650Stats.h>

inline void logReads(batt_read_t);

void battery_actor (void * params){
    
    Battery18650Stats battery(GPIO_ANALOG_BATTERY_METER);
    batt_read_t read;

    read.volts = battery.getBatteryVolts();
    read.percent = battery.getBatteryChargeLevel(true);
    xQueueSend(queue_from_battery, &read, 100);
    logReads(read);
    vTaskDelete(NULL); //ending actor
}

inline void logReads(batt_read_t read){
    char logMsg[16];

    sprintf(logMsg, "v: %fv", read.volts);
    doLog(logMsg);
    sprintf(logMsg, "charge: %d%", read.percent);
    doLog(logMsg);
}