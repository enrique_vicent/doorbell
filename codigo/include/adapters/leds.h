#ifndef apapter_leds_h
#define apapter_leds_h
#include <Arduino.h>
#include "config.h"
#include "coreApi.h"
#include "adapters/log.h"

void leds_actor (void * parameters){
    led_options_t command;

    pinMode(GPIO_LED_ACTIVE, OUTPUT);
    pinMode(GPIO_LED_OPENING, OUTPUT);
    pinMode(GPIO_LED_ON, OUTPUT);
    auto blinkPeriod = portMAX_DELAY; //infinite
    auto blinkingMode = 0;
    auto blinkStatus = 0;

    for(;;){

        bool wasReceived = pdTRUE == xQueueReceive(queue_to_led, &command, blinkPeriod);
        if (wasReceived){
            switch (command)
            {
                case allOff: 
                    doLog ("all off"); 
                    digitalWrite (GPIO_LED_ACTIVE, LOW); 
                    digitalWrite(GPIO_LED_OPENING, LOW); 
                    digitalWrite(GPIO_LED_ON, LOW); 
                    blinkPeriod = portMAX_DELAY;
                    break; 
                case allOn: 
                    doLog ("all on"); 
                    digitalWrite (GPIO_LED_ACTIVE, HIGH); 
                    digitalWrite(GPIO_LED_OPENING, HIGH); 
                    digitalWrite(GPIO_LED_ON, HIGH); 
                    blinkPeriod = portMAX_DELAY;
                    break;
                case activeOn: 
                    doLog ("activa on"); 
                    digitalWrite (GPIO_LED_ACTIVE, HIGH);
                    blinkPeriod = portMAX_DELAY;
                    break;
                case activeOff: 
                    doLog ("activa off"); 
                    digitalWrite (GPIO_LED_ACTIVE, LOW);
                    blinkPeriod = portMAX_DELAY;
                    break;
                case solenoidOn: 
                    doLog ("solenoid on"); 
                    digitalWrite(GPIO_LED_OPENING, HIGH);
                    blinkPeriod = portMAX_DELAY;
                    break;
                case solenoidOff: 
                    doLog ("solenoid off"); 
                    digitalWrite(GPIO_LED_OPENING, LOW) ;
                    blinkPeriod = portMAX_DELAY;
                    break;   
                case networkOn: 
                    doLog ("network on");
                    digitalWrite(GPIO_LED_ON, HIGH);
                    blinkPeriod = portMAX_DELAY;
                    break;
                case networkOff: 
                    doLog ("network off");
                    digitalWrite(GPIO_LED_ON, LOW);
                    blinkPeriod = portMAX_DELAY;
                    break;
                case OTAListening: 
                    blinkPeriod = 250;
                    blinkingMode = 1;
                    blinkStatus = 0;
                    digitalWrite (GPIO_LED_ACTIVE, LOW); 
                    digitalWrite(GPIO_LED_OPENING, LOW); 
                    digitalWrite(GPIO_LED_ON, LOW); 
                    break;
                case OTAUpdating: 
                    blinkPeriod = 100;
                    blinkingMode = 2;
                    blinkStatus = 0;
                    digitalWrite (GPIO_LED_ACTIVE, LOW); 
                    digitalWrite(GPIO_LED_OPENING, LOW); 
                    digitalWrite(GPIO_LED_ON, HIGH); 
                    break;
                default: 
                    doLog ("bad!!!"); 
                    break;
            }
        } else { //no mesage=> blinking scenarios
            switch (blinkingMode){
            case 1:
                switch (blinkStatus){
                case 0:
                    digitalWrite(GPIO_LED_ON, HIGH); 
                    break;
                case 1:
                    digitalWrite(GPIO_LED_ON, LOW); 
                    break;
                default:
                    break;
                }
                blinkStatus = (blinkStatus+1) % 2;
                break;
            case 2:
                switch (blinkStatus){
                case 0:
                    digitalWrite(GPIO_LED_ACTIVE, LOW); 
                    break;
                case 1:
                    digitalWrite(GPIO_LED_ACTIVE, HIGH); 
                    break;
                }
                blinkStatus = (blinkStatus+1) % 2;
                break;
            }

        }
    }
}

#endif