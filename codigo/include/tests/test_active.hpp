#include <Arduino.h>
#include <unity.h>
#include "coreApi.h"
#include "tests/utils/testUtils.hpp"
#include "core/core.h"

void goActiveState  (bool wifi){
    //GIVEN start status
    test::beforeEach();

    if (wifi){
        //and wifi connected is injected
        test::send2Queue(true, queue_from_wifi);
    }

    //WHEN active raised
    test::send2Queue(true, queue_from_bell);

    //AND the hk sends and ack (if wifi)
    if (wifi){
        test::send2Queue(hk_events_t::update_ok, queue_from_homekit);
    }
    //THEN 2 watchdog (brust and active) are raised
    watchdog_enum dogNameCreated;
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_watchdog__reset, &dogNameCreated, 100), "watchdog no creado");
    TEST_ASSERT_EQUAL_MESSAGE (watchdog_enum::BRUST_TIMEOUT, dogNameCreated, "first should be brust");
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_watchdog__reset, &dogNameCreated, 100), "watchdog 2 no creado");
    TEST_ASSERT_EQUAL_MESSAGE (watchdog_enum::ACTIVE_TIMEOUT, dogNameCreated, "second should be active");
    delay(10);

    //AND a hk update (ring) exists
    test::assertHkRing();

    //WHen 1º watchdog brust triggers
    test::send2Queue (watchdog_enum::BRUST_TIMEOUT, queue_from_watchdog__triggered );

    //AND no hibernation happens
    bool sleepMsg;
    TEST_ASSERT_FALSE_MESSAGE(xQueueReceive(queue_to_sleep, &sleepMsg, 100), "se ha ibernado!!");
    delay(10);

}

const bool doubleTimeout = true;
const bool regularTimeout = false;
void doTimeoutFromActive (bool doubleTimeout) {
    //GIVEN 2 empty recipients for messagess
    watchdog_enum watchdogTriggered;
    bool sleepMsg;

    //WHEN watchdog active triggers
    test::send2Queue (watchdog_enum::ACTIVE_TIMEOUT, queue_from_watchdog__triggered );

    //THEN 2º watchdog brust resets
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_watchdog__reset, &watchdogTriggered, 100), "watchdog 3 no creado");
    TEST_ASSERT_EQUAL_MESSAGE (watchdog_enum::BRUST_TIMEOUT, watchdogTriggered, "third should be brust");

    if (doubleTimeout){
        //WHen 2º watchdog brust triggers
        test::send2Queue (watchdog_enum::BRUST_TIMEOUT, queue_from_watchdog__triggered );
    }
    
    //THEN we go to sleep
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_sleep, &sleepMsg, 100), "no se ha hibernado");
    TEST_ASSERT_TRUE(sleepMsg);
    delay(10);
}

void doOpenTheDoor(int acks=2){
    //and a (remote) coil activaton
    test::send2Queue(hk_events_t::open_door_comand, queue_from_homekit);

    //then an hk update exists 
    test::assertHkSolenoid(true);

    //and a coil on update message exist
    test::assertSolenoidActivation();
    
    //When the coil is deactivated
    test::send2Queue(solenoid_operations_t::solenoid_ended, queue_from_solenoid);

    //THEN a coil update message exist
    test::assertHkSolenoid(false);

    //When 2 acks are received (aditional for te solenoid operations)
    for (int i=0; i<acks; i++){
        test::send2Queue(hk_events_t::update_ok, queue_from_homekit);
    }
}

void test_active_nowifi(void){  
    //GiVEN active State
    goActiveState(false);  

    //THEN a timeout may occur
    doTimeoutFromActive (doubleTimeout);

}

void test_active_no_hk(void){  
    //GiVEN active State
    goActiveState(true);  

    //THEN a timeout may occur
    doTimeoutFromActive (doubleTimeout); 
}

void test_active(void){  
    //GiVEN active State
    goActiveState(true);  

    //and a hk_ack inserted for the ring update
    test::send2Queue(hk_events_t::update_ok, queue_from_homekit);

    //THEN a timeout may occur
    doTimeoutFromActive(regularTimeout); 
}

void test_coil(void){
    //GIVEN activeState
    goActiveState(true);

    //AND we open the door
    doOpenTheDoor();

    //THEN the system can resume sleeping
    doTimeoutFromActive(regularTimeout);
}

void test_coil_mid_hk_timeout(){
    //GIVEN activeState
    goActiveState(true);

    //AND we open the door but still is opening
    doOpenTheDoor(1);

    //watchdog active triggers
    test::send2Queue (watchdog_enum::ACTIVE_TIMEOUT, queue_from_watchdog__triggered );

    //THEN the system do not go to sleep
    bool sleepMsg;
    TEST_ASSERT_FALSE_MESSAGE(xQueueReceive(queue_to_sleep, &sleepMsg, 100), "se ha ibernado!!");
    delay(10);

    //but WHEN brust watchdog triggers
    test::send2Queue (watchdog_enum::BRUST_TIMEOUT, queue_from_watchdog__triggered );

    //THEN the system do not go to sleep
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_sleep, &sleepMsg, 100), "no se ha ibernado!!");
}

void test_coil_mid_timeout(){
    //GIVEN activeState
    goActiveState(true);

    //And a request to open the door
    test::send2Queue(hk_events_t::open_door_comand, queue_from_homekit);

    //AND Solenoid activates (hk ack) 
    test::send2Queue(hk_events_t::update_ok, queue_from_homekit);

    //And NO solenoid deativation message yet received
    delay(1);

    //AND the timeout occur:
    test::send2Queue (watchdog_enum::ACTIVE_TIMEOUT, queue_from_watchdog__triggered );
    test::send2Queue (watchdog_enum::BRUST_TIMEOUT, queue_from_watchdog__triggered );

    //THEN we cannot go sleep
    bool sleepMsg;
    TEST_ASSERT_FALSE_MESSAGE(xQueueReceive(queue_to_sleep, &sleepMsg, 100), "se ha hibernado");

    //When the coil is deactivated
    test::send2Queue(solenoid_operations_t::solenoid_ended, queue_from_solenoid);
    test::send2Queue(hk_events_t::update_ok, queue_from_homekit);

    //THEN we cannot go sleep
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_sleep, &sleepMsg, 100), "no se ha hibernado");

    //AND the Brust timeout was armed
    watchdog_enum dogNameCreated;
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_watchdog__reset, &dogNameCreated, 100), "watchdog no creado");
    TEST_ASSERT_EQUAL_MESSAGE (watchdog_enum::BRUST_TIMEOUT, dogNameCreated, "should be brust");

}

void test_coil_leds(void){
    //GIVEN activeState
    goActiveState(true);
    
    //THEN just active and networkd led is on
    test::assertLedRequest(led_options_t::networkOn);
    test::assertLedRequest(led_options_t::activeOn);
    test::assertNoLedPendingCommand();

    //WHEN we open the door
    doOpenTheDoor();

    //THEN coil led blinks
    test::assertLedRequest(led_options_t::solenoidOn);
    test::assertLedRequest(led_options_t::solenoidOff);
    test::assertNoLedPendingCommand();

    //When the system resume sleeping
    doTimeoutFromActive(regularTimeout);

    //THEN active led is off
    test::assertLedRequest(led_options_t::activeOff);
    test::assertNoLedPendingCommand();
}

