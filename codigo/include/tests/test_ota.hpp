#include <Arduino.h>
#include <unistd.h>
#include "coreApi.h"
#include "tests/utils/testUtils.hpp"
#include "core/core.h" //sut

void test_OTA(){
    //GIVEN start status
    test::beforeEach();
    ota_commands_t ota_rec;
    led_options_t led_rec;
    BaseType_t status;

    //when flasing starts without starting ota
    test::send2Queue(ota_updates_t::ota_updating_firmware, queue_from_ota);

    //then nothing occurs
    status = xQueueReceive(queue_to_led, &led_rec, 100);
    TEST_ASSERT_EQUAL_MESSAGE(pdFALSE, status, "should not react to flash update_c");

    //when ota Starts;
    test::send2Queue(ota_request_t::ota_start, queue_from_ota_friend);

    //then ota starts;
    status = xQueueReceive(queue_to_ota, &ota_rec,100);
    TEST_ASSERT_EQUAL_MESSAGE(pdTRUE, status, "ota not activated");
    TEST_ASSERT_EQUAL_MESSAGE(ota_commands_t::ota_begin, ota_rec, "ota not proper activated");
    
    //and lights go on
    status = xQueueReceive(queue_to_led, &led_rec,100);
    TEST_ASSERT_EQUAL_MESSAGE(pdTRUE, status, "leds not blinking ota");
    TEST_ASSERT_EQUAL_MESSAGE(led_options_t::OTAListening, led_rec, "leds not blinking ota properly");

    //when flasing starts now
    test::send2Queue(ota_updates_t::ota_updating_firmware, queue_from_ota);

    //then blinking is updated
    status = xQueueReceive(queue_to_led, &led_rec,100);
    TEST_ASSERT_EQUAL_MESSAGE(pdTRUE, status, "leds not blinking flashing");
    TEST_ASSERT_EQUAL_MESSAGE(led_options_t::OTAUpdating, led_rec, "leds not blinking flashing properly");

}