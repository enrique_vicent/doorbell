#pragma once

#include <Arduino.h>
#include <unity.h>
#include "coreApi.h"
#include "core/core.h"
#include "config.h"

namespace test {

    void clearToQueues (){ //todo: move to its file
        char buffer [127];
        const char * queueNames[] = {"queue_to_led", "queue_to_solenoid", "queue_to_homekit", "queue_to_sleep", "queue_to_watchdog__reset"};
        auto queues = {queue_to_led, queue_to_solenoid, queue_to_homekit, queue_to_sleep, queue_to_watchdog__reset};
        int index = 0;
        for (auto q : queues){
            while (pdTRUE == xQueueReceive(q, &buffer,0)){
                Serial.print("testWarning: cola con contenido ");
                Serial.println(queueNames[index]);
            }
            index++;
        }
    }

    void beforeEach(){
        destroyCore();
        clearToQueues();    
        core();
    }

    template <class T>
    void send2Queue(T msg, QueueHandle_t queue){
        xQueueSend(queue, &msg, 0);
        delay(10);
    }

    void injectBatteryRead(int percent, double volts){
        batt_read_t batt_read;
        batt_read.percent=percent;
        batt_read.volts=volts;
        send2Queue<batt_read_t>(batt_read, queue_from_battery);
    }

    void assertHkRing(){
        hk_update_t ring;
        TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_homekit, &ring, 0), "no ring to hk");
        TEST_ASSERT_TRUE_MESSAGE(ring.ringing, "update is not ringing");
        TEST_ASSERT_FALSE(ring.updateBatt);
        TEST_ASSERT_FALSE(ring.updateSolenoidStatus);
    }

    void assertHkSolenoid(bool open){
        hk_update_t solenoid;
        TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_homekit, &solenoid, 0), open?"no hk door opening":"no hk door closing");
        TEST_ASSERT_FALSE(solenoid.ringing);
        TEST_ASSERT_FALSE(solenoid.updateBatt);
        TEST_ASSERT_TRUE(solenoid.updateSolenoidStatus);
        TEST_ASSERT_EQUAL(open, solenoid.doorIsOpen);
    }

    void assertSolenoidActivation(){
        solenoid_id_t msg;
        TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_solenoid, &msg, 0), "no solenoid activation");
        TEST_ASSERT_EQUAL(SOLENOID_OPEN_FOR_MILIS, msg.forMilis);
        TEST_ASSERT_TRUE(msg.open);
    }

    void assertNoLedPendingCommand(){
        led_options_t nada;
        auto read = xQueueReceive(queue_to_led, &nada, 0);
        if(read){
            Serial.print("operacion led pendiente para: ");
            Serial.println( nada );
        }
        TEST_ASSERT_FALSE_MESSAGE( read, "operacion led pendiente");
    }

    void assertLedRequest(led_options_t req){
        led_options_t cmd;
        TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_led, &cmd, 0), "no led command");
        TEST_ASSERT_EQUAL_MESSAGE(req, cmd, "not the expeceted led");
    }
}

