#include <Arduino.h>
#include <unity.h>
#include "coreApi.h"
#include "tests/utils/testUtils.hpp"
#include "core/core.h"



void test_battery_poll_ok(void){    
    //GIVEN start status
    test::beforeEach();

    //WHEN battery read is injected
    test::injectBatteryRead(90,4.1);

    //and wifi connected is injected
    test::send2Queue(true, queue_from_wifi);

    //and battery read is sent and ack injected
    test::send2Queue(hk_events_t::update_ok, queue_from_homekit);

    //THEN brust watchdog created
    watchdog_enum dogNameCreated;
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_watchdog__reset, &dogNameCreated, 100), "watchdog no creado");
    TEST_ASSERT_EQUAL_MESSAGE (watchdog_enum::BRUST_TIMEOUT, dogNameCreated, "should be brust");

    //THEN leds indicate network connection
    test::assertLedRequest(led_options_t::networkOn);
    
    //THEN the hk was updated
    hk_update_t hk_update;
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_homekit, &hk_update, 100), "hk no actualizado");
    TEST_ASSERT_TRUE(hk_update.updateBatt);
    TEST_ASSERT_EQUAL_FLOAT(90.0, hk_update.battRead);
    TEST_ASSERT_FALSE(hk_update.updateSolenoidStatus);
    TEST_ASSERT_FALSE(hk_update.ringing);
    delay(10);

    //and the systen requested go to sleep
    bool sleepMsg;
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_sleep, &sleepMsg, 100), "no se ha hibernado");
    TEST_ASSERT_TRUE(sleepMsg);
    delay(10);

    //and no led commanded to light
    test::assertNoLedPendingCommand();
};

void test_battery_poll_noWifi(void){
    
    //GIVEN start status
    test::beforeEach();

    //WHEN battery read is injected
    test::injectBatteryRead(90,3.9);

    //and no wifi connected is injected
    //and battery read is sent and ack injected
    test::send2Queue(hk_events_t::update_ok, queue_from_homekit);

    //and a timeout (brust) raises
    test::send2Queue(watchdog_enum::BRUST_TIMEOUT, queue_from_watchdog__triggered);

    //and the systen requested go to sleep
    bool sleepMsg;
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_sleep, &sleepMsg, 100), "no se ha hibernado");
    TEST_ASSERT_TRUE(sleepMsg);
    delay(10);
};



void test_battery_sleep_while_pending(void){    
    //GIVEN start status
    test::beforeEach();

    //WHEN battery read is injected
    test::injectBatteryRead(90,3.9);

    //and wifi connected is injected
    test::send2Queue(true, queue_from_wifi);

    //and a timeout (brust) raises before sending
    test::send2Queue(watchdog_enum::BRUST_TIMEOUT, queue_from_watchdog__triggered);

    //and battery read is sent and ack injected
    test::send2Queue(hk_events_t::update_ok, queue_from_homekit);


    //THEN the hk was updated
    hk_update_t hk_update;
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_homekit, &hk_update, 100), "hk no actualizado");
    TEST_ASSERT_TRUE(hk_update.updateBatt);
    TEST_ASSERT_EQUAL_FLOAT(90.0, hk_update.battRead);
    TEST_ASSERT_FALSE(hk_update.updateSolenoidStatus);
    TEST_ASSERT_FALSE(hk_update.ringing);
    delay(10);

    //and the systen requested go to sleep
    bool sleepMsg;
    TEST_ASSERT_TRUE_MESSAGE(xQueueReceive(queue_to_sleep, &sleepMsg, 100), "no se ha hibernado");
    TEST_ASSERT_TRUE(sleepMsg);
    delay(10);
};
